# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 17:01:17 2018

@author: Administrator
"""

import pandas as pd


def ohlc_bar(file_loc, time_frame = "1min"):
    
    df = pd.read_csv(file_loc, header = None, sep = ";")
    df.columns = ["Datetime", "Open", "High", "Low", "Close"]
    df["Datetime"] = pd.to_datetime(df["Datetime"])
    
    if time_frame != "1min":
        
        open_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq = str(time_frame) + "min"))["Open"].first())
        high_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq = str(time_frame) + "min"))["High"].max())
        low_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq = str(time_frame) + "min"))["Low"].min())        
        close_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq = str(time_frame) + "min"))["Close"].last())
        
        return(pd.concat([open_price, high_price, low_price, close_price], axis = 1)).dropna()
    
    return df.set_index("Datetime")


def tick(file_loc):
    
    df = pd.read_csv(file_loc, header = None, sep = ";")
    df.columns = ["Datetime", "Bid", "Ask"]
    df["Datetime"] = pd.to_datetime(df["Datetime"])
    
    return df.set_index("Datetime")


def ohlc_bar_serv(serv_result):
    
    df = pd.DataFrame(serv_result)
    
    df["Datetime"] = pd.to_datetime(df[0].apply(str) + " "+ df[1].apply(str))
    df.rename(columns = {2: "Open", 3: "High", 4: "Low", 5: "Close"}, inplace = True)
    df.drop([0, 1], axis = 1, inplace = True)
    df.sort_values("Datetime", inplace = True)
    df.set_index("Datetime", inplace = True)
    
    for col in df.columns:
        
        df[col] = df[col].apply(float)
    
    return df


def tick_serv(serv_result):
    
    df = pd.DataFrame(serv_result)
    
    df["Datetime"] = pd.to_datetime(df[0].apply(str) + " "+ df[1].apply(str))
    df.rename(columns = {2: "Ask", 3: "Bid"}, inplace = True)
    df.drop([0, 1], axis = 1, inplace = True)
    df.sort_values("Datetime", inplace = True)
    df.set_index("Datetime", inplace = True)
    
    for col in df.columns:
        
        df[col] = df[col].apply(float)
    
    return df    