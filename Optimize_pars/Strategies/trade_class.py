# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 17:07:48 2018

@author: Moe Yang
"""

# =============================== settings ====================================

bar_loc = "D:\\Google Drive\\work\\Moe\\Analysis Packages\\Data\\mt4csv\\"
tick_loc = "D:\\Google Drive\\work\Moe\\Analysis Packages\\Data\\tick_csv\\"
DATABASES = {'DBNAME': "bfjfunds",
             'DBUSER': "bfjfunds",
             'DBPASSWORD': "BFJFunds2017",
             'DBHOST': "bfjfunds.chkx8ynoq5pn.ap-southeast-2.rds.amazonaws.com",
             'DBPORT': "5432"}

size_5 = ["EURUSD", "GBPUSD", "AUDUSD", "USDCHF", "NZDUSD", "USDCAD", "AUDNZD",
          "EURCHF", "EURNZD", "EURGBP", "EURCAD", "GBPAUD", "EURAUD", "GBPCAD",
          "AUDCAD", "USDCNH"]
size_4 = ["GAS-C"]
size_3 = ["USDJPY", "EURJPY", "GBPJPY", "CADJPY", "CHFJPY", "VIX", "CL-OIL", "USDX",
          "NG-C", "XAGUSD"]
size_2 = ["XAUUSD", "BTCUSD", "DJ30", "SP500"]
size_1 = []
size_0 = ["CHINA50"]

symbol_size = {**{a: 100000 for a in size_5},
               **{b: 10000 for b in size_4},
               **{c: 1000 for c in size_3},
               **{d: 100 for d in size_2},
               **{e: 10 for e in size_1},
               **{f: 1 for f in size_0}}

price_format = {**{a: "0.00000" for a in size_5},
                **{b: "0.0000" for b in size_4},
                **{c: "0.000" for c in size_3},
                **{d: "0.00" for d in size_2},
                **{e: "0.0" for e in size_1},
                **{f: "0" for f in size_0}}

# =============================================================================


import re
import warnings
from math import pi
from os import listdir

import bokeh.palettes as bp
import pandas as pd
import psycopg2
from bokeh.embed import components
from bokeh.models import Range1d, HoverTool
from bokeh.plotting import figure, show, output_file, reset_output

from Optimize_pars.Strategies import techIndicators as ti

cct = ti.cum_counter


class Price_chart:
    ''' Price_chart class represents all available prices bar & tick prices for certain product/symbol '''

    def __init__(self, symbol_name, spread):

        ''' create a new Price_chart for certain symbol '''
        self.name = symbol_name

        ''' detect all available file '''
        self.mt4bar_file_lst = [f for f in listdir(bar_loc) if self.name in f]
        self.tick_file_lst = [f for f in listdir(tick_loc) if self.name in f]

        ''' initial chart_dict for different timeframe '''
        self.chart_dict = {}

        ''' initial features of symbol '''
        self.size = symbol_size[symbol_name]
        self.spread = spread

        self.cunn = None

        self.tf_int2freq = {1: "1min", 5: "5min", 10: "10min", 15: "15min",
                            30: "30min", 60: "1H", 240: "4H", 1440: "D",
                            10080: "W", 43200: "MS"}
        self.tf_int2str = {1: "1m", 60: "1h", 240: "4h", 1440: "1d", 10080: "1w", 43200: "1mn"}

    def __str__(self):
        ''' describe the available time frame for the symbol '''
        return "Price_chart for " + self.name

    def conn_serv(self):

        try:
            conn = psycopg2.connect(dbname=DATABASES['DBNAME'],
                                    user=DATABASES['DBUSER'],
                                    password=DATABASES['DBPASSWORD'],
                                    host=DATABASES['DBHOST'],
                                    port=DATABASES['DBPORT'])
            print("connecting")
        except:
            print("Error on connecting to PostgreSQL")

        self.cunn = conn

    def init_charts(self):

        self.chart_dict = {}

    def drop_charts(self, time_frame):

        if time_frame not in self.chart_dict.keys():
            return warnings.warn("Chart doesn't exist!")

        del self.chart_dict[time_frame]

    def fetch_bar_serv(self, time_frame_lst, IsTemp=False):

        if self.cunn == None:
            return warnings.warn("connection not established yet! call conn_serv() to create connection")

        for time_frame in time_frame_lst:

            if IsTemp:

                sql_ohlc = '''
                          select record_date, record_time, open, high, low, close
                          from bfj_price_ohlc_''' + self.tf_int2str[time_frame] + "_temp" \
                                                                                  '''
                                                                                  where symbol = %s and broker_id = 1'''
            else:
                sql_ohlc = '''
                           select record_date, record_time, open, high, low, close
                           from bfj_price_ohlc_''' + self.tf_int2str[time_frame] + \
                           '''
                           where symbol = %s and broker_id = 1'''

            cur = self.cunn.cursor()
            cur.execute(sql_ohlc, [self.name])
            price_df = pd.DataFrame(cur.fetchall())
            dt = pd.to_datetime(price_df[0].apply(str) + " " + price_df[1].apply(str))
            price_df.drop([0, 1], axis=1, inplace=True)
            price_df.columns = ["Open", "High", "Low", "Close"]

            for col in price_df.columns:
                price_df[col] = price_df[col].apply(float)

            price_df["Datetime"] = dt
            price_df.sort_values("Datetime", inplace=True)
            price_df.reset_index(drop=True, inplace=True)

            self.chart_dict[time_frame] = price_df

    def fetch_tick_serv(self, IsTemp=False):

        if self.cunn == None:
            return warnings.warn("connection not established yet! call conn_serv() to create connection")

        if IsTemp:
            sql_tick = '''
                       select record_date, record_time, ask, bid
                       from bfj_price_tick_temp
                       where symbol = %s and broker_id = 1'''

        else:
            sql_tick = '''
                       select record_date, record_time, ask, bid
                       from bfj_price_tick
                       where symbol = %s and broker_id = 1'''

        cur = self.cunn.cursor()
        cur.execute(sql_tick, [self.name])
        price_df = pd.DataFrame(cur.fetchall())
        dt = pd.to_datetime(price_df[0].apply(str) + " " + price_df[1].apply(str))
        price_df.drop([0, 1], axis=1, inplace=True)
        price_df.columns = ["Ask", "Bid"]

        for col in price_df.columns:
            price_df[col] = price_df[col].apply(float)

        price_df["Datetime"] = dt
        price_df.sort_values("Datetime", inplace=True)
        price_df.reset_index(drop=True, inplace=True)

        self.chart_dict[0] = price_df

    def fetch_mt4bar(self, time_frame):

        file_name = self.name + str(time_frame) + ".csv"

        if file_name not in self.mt4bar_file_lst:
            return warnings.warn("Current time_frame is unavailable!")

        bar_df = pd.read_csv(bar_loc + file_name, header=None)
        bar_df.columns = ["Date", "Time", "Open", "High", "Low", "Close", "Volume"]
        bar_df["Datetime"] = pd.to_datetime(bar_df["Date"] + " " + bar_df["Time"])

        self.chart_dict[time_frame] = bar_df

        return bar_df

    def fetch_tick(self, time_str):

        file_name = self.name + time_str + ".csv"

        if self.name not in self.tick_file_lst:
            return warnings.warn("Tick price is not available!")

        tick_df = pd.read_csv(tick_loc + file_name, header=None)
        tick_df.columns = ["Datetime", "Bid", "Ask"]

        self.chart_dict[0] = tick_df

        return tick_df

    def fetch_all(self, auto_fill_miss=False, fetch_tick=False):

        for f in self.mt4bar_file_lst:
            bar_df = pd.read_csv(bar_loc + f, header=None)
            bar_df.columns = ["Date", "Time", "Open", "High", "Low", "Close", "Volume"]
            bar_df["Datetime"] = pd.to_datetime(bar_df["Date"] + " " + bar_df["Time"])

            self.chart_dict[int(f.replace(".csv", "").replace(self.name, ""))] = bar_df

        if fetch_tick:

            for f in self.tick_file_lst:
                tick_df = pd.read_csv(tick_loc + f, header=None)
                tick_df.columns = ["Datetime", "Bid", "Ask"]

                self.chart_dict[0] = tick_df

        if auto_fill_miss:
            miss_tfs = [x for x in [1, 5, 15, 30, 60, 120, 180, 240, 1440] if x not in self.chart_dict.keys()]

            based_tf = min(self.chart_dict.keys())

            self.trans_lst(based_tf, miss_tfs)

            self.trans_tick(based_tf)

        return "The following charts already fetched: " + str(list(self.chart_dict.keys()))

    def trans_bar(self, from_tf, to_tf):

        if from_tf not in self.chart_dict.keys():
            return warnings.warn("Original time_frame chart not exist! Fetch the chart before transfer")

        if from_tf > to_tf:
            return warnings.warn("Original time_frame is higher than destinated time_frame")

        df = self.chart_dict[from_tf]

        if from_tf > 0:

            open_price = pd.Series(
                df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Open"].first())
            high_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["High"].max())
            low_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Low"].min())
            close_price = pd.Series(
                df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Close"].last())

        else:

            open_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Bid"].first(),
                                   name="Open")
            high_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Bid"].max(),
                                   name="High")
            low_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Bid"].min(),
                                  name="Low")
            close_price = pd.Series(df.set_index("Datetime").groupby(pd.Grouper(freq=str(to_tf) + "min"))["Bid"].last(),
                                    name="Close")

        result_chart = pd.concat([open_price, high_price, low_price, close_price], axis=1).dropna().reset_index()

        if to_tf in self.chart_dict.keys():

            warnings.warn("Destinitic time_frame already exist! Drop the chart before transfer")

        else:

            self.chart_dict[to_tf] = result_chart

        return result_chart

    def trans_tick(self, from_tf, ori_price_type="Bid"):

        if from_tf not in self.chart_dict.keys():
            return warnings.warn("Original time_frame chart not exist! Fetch the chart before transfer")

        df = self.chart_dict[from_tf].set_index("Datetime").T.unstack().reset_index()
        df = df.loc[df["level_1"].isin(["Open", "High", "Low", "Close"]), ["Datetime", 0]]
        df.rename(columns={0: ori_price_type}, inplace=True)

        if ori_price_type == "Bid":

            df["Ask"] = df["Bid"] + self.spread / self.size

        elif ori_price_type == "Ask":

            df["Bid"] = df["Ask"] - self.spread / self.size

        if 0 in self.chart_dict.keys():

            warnings.warn("Tick price already exist! Drop the chart before transfer")

        else:

            self.chart_dict[0] = df

        return df

    def trans_lst(self, from_tf, to_tf_lst):

        tf_lst = [x for x in to_tf_lst if x > from_tf]

        for tf in tf_lst:
            self.trans_bar(from_tf, tf)

        return "Update time_frame based on time_frame = " + str(from_tf)

    def get_mt4bar(self, time_frame):

        if time_frame not in self.chart_dict.keys():
            return warnings.warn("Current time_frame is not fetched yet!")

        return self.chart_dict[time_frame]

    def get_tick(self):

        if 0 not in self.chart_dict.keys():
            return warnings.warn("Tick price is not fetched yet!")

        return self.chart_dict[0]

    def get_fetched_charts(self):

        return list(self.chart_dict.keys())

    def get_size(self):

        return self.size

    def get_bar_tick(self, time_frame, ori_price_type="Bid", from_price="Close"):

        if time_frame not in self.chart_dict.keys():
            return warnings.warn("Current time_frame is not fetched yet!")

        tick_dict = {ori_price_type: self.chart_dict[time_frame][from_price],
                     "Datetime": self.chart_dict[time_frame]["Datetime"]}

        if ori_price_type == "Bid":

            tick_dict["Ask"] = tick_dict[ori_price_type] + self.spread / self.size

        elif ori_price_type == "Ask":

            tick_dict["Bid"] = tick_dict[ori_price_type] - self.spread / self.size

        return pd.concat(tick_dict, axis=1)


class Trade_book:

    def __init__(self, symbol_name, ini_balance=10000):

        self.live_orders = {}
        self.closed_orders = {}
        self.pend_orders = {}

        self.float_pnl = {}
        self.float_pos = {}

        self.size = symbol_size[symbol_name]

        self.current_time = 0

        self.current_ask = 0
        self.current_bid = 0

        self.index = 0

        self.name = symbol_name
        self.ini_balance = ini_balance

    def counter(self):

        self.index += 1

        return self.index

    def update(self, current_time, current_ask, current_bid):

        self.current_time = current_time
        self.current_ask = current_ask
        self.current_bid = current_bid

        self.tpsl_all()
        self.open_pend_order()

    def get_hist_orders(self, order_status, mode="df"):

        order_pools_dict = {"live": self.live_orders, "closed": self.closed_orders,
                            "pend": self.pend_orders}

        if mode == "df":

            return pd.DataFrame(order_pools_dict[order_status]).T

        elif mode == "dict":

            return order_pools_dict[order_status]

    def get_closed_pnl(self):

        self.get_hist_orders("closed").set_index("close time") \
            .reset_index()["pnl"].cumsum() \
            .plot(title=self.name + " closed pnl")

    def get_total_pnl(self):

        return sum([v["pnl"] for v in self.closed_orders.values()])

    def get_trade_len(self):

        return len(self.closed_orders)

    def get_pl_ratio(self):

        loss = sum([v["pnl"] for v in self.closed_orders.values() if v["pnl"] < 0])

        if loss == 0:
            warnings.warn("No loss pnl!")

            return 0

        return - sum([v["pnl"] for v in self.closed_orders.values() if v["pnl"] > 0]) / loss

    def get_win_rate(self):

        total_order = len(self.closed_orders)

        if total_order == 0:
            warnings.warn("No closed order!")

            return 0

        return len([v for v in self.closed_orders.values() if v["pnl"] > 0]) / total_order

    def get_float_pnl(self):

        pd.Series(self.float_pnl).plot(title=self.name + " float pnl")

    def get_float_pos(self):

        pd.Series(self.float_pos).plot(title=self.name + " float pos")

    def get_sharp_ratio(self, time_unit=1440):

        float_pnl_s = pd.Series(self.float_pnl) + self.ini_balance

        fp_std = float_pnl_s.pct_change().std()

        if len(float_pnl_s) <= 1 or fp_std == 0:
            warnings.warn("No enough Float PNL points")

            return 0

        fp_time_rng = (float_pnl_s.index[-1] - float_pnl_s.index[0]).total_seconds() / 60 / time_unit

        fp_mean = (float_pnl_s.iloc[-1] / self.ini_balance - 1) / fp_time_rng

        return fp_mean / fp_std

    def get_sortino_ratio(self, time_unit=1440):

        float_pnl_s = pd.Series(self.float_pnl) + self.ini_balance

        if len(float_pnl_s) <= 1:
            warnings.warn("No enough Float PNL points")

            return 0

        fp_semi_std = ((float_pnl_s[float_pnl_s < 0] ** 2).sum() / len(float_pnl_s)) ** (1 / 2)

        fp_time_rng = (float_pnl_s.index[-1] - float_pnl_s.index[0]).total_seconds() / 60 / time_unit

        fp_mean = (float_pnl_s.iloc[-1] / self.ini_balance - 1) / fp_time_rng

        return fp_mean / fp_semi_std

    def send_market_order(self, lots, direc, tp_price=None, sl_price=None,
                          trade_status="open", mark=None):

        open_price = self.current_ask if direc == "buy" else self.current_bid

        order_number = self.counter()

        self.live_orders[order_number] = {"type": direc, "lots": lots, "open time": self.current_time,
                                          "open price": open_price, "trade status": trade_status,
                                          "TP": tp_price, "SL": sl_price, "order #": order_number,
                                          "Note": mark}

    def send_pend_order(self, lots, direc, open_price, tp_price=None, sl_price=None,
                        trade_status="pending", mark=None):

        order_number = self.counter()

        self.pend_orders[order_number] = {"type": direc, "lots": lots, "open time": self.current_time,
                                          "open price": open_price, "trade status": trade_status,
                                          "TP": tp_price, "SL": sl_price, "order #": order_number,
                                          "Note": mark}

    def open_pend_order(self, trade_status="open"):

        for i in list(self.pend_orders.keys()):

            if self.pend_orders[i]["type"] == "buy limit":

                if self.current_ask <= self.pend_orders[i]["open price"]:
                    self.pend_orders[i]["type"] = "buy"
                    self.pend_orders[i]["open time"] = self.current_time
                    self.pend_orders[i]["order #"] = self.counter()
                    self.pend_orders[i]["trade status"] = "open"
                    self.live_orders[i] = self.pend_orders[i]

                    del self.pend_orders[i]

            elif self.pend_orders[i]["type"] == "sell limit":

                if self.current_bid >= self.pend_orders[i]["open price"]:
                    self.pend_orders[i]["type"] = "sell"
                    self.pend_orders[i]["open time"] = self.current_time
                    self.pend_orders[i]["order #"] = self.counter()
                    self.pend_orders[i]["trade status"] = "open"
                    self.live_orders[i] = self.pend_orders[i]

                    del self.pend_orders[i]

            elif self.pend_orders[i]["type"] == "buy stop":

                if self.current_ask >= self.pend_orders[i]["open price"]:
                    self.pend_orders[i]["type"] = "buy"
                    self.pend_orders[i]["open time"] = self.current_time
                    self.pend_orders[i]["order #"] = self.counter()
                    self.pend_orders[i]["trade status"] = "open"
                    self.live_orders[i] = self.pend_orders[i]

                    del self.pend_orders[i]

            elif self.pend_orders[i]["type"] == "sell stop":

                if self.current_bid <= self.pend_orders[i]["open price"]:
                    self.pend_orders[i]["type"] = "sell"
                    self.pend_orders[i]["open time"] = self.current_time
                    self.pend_orders[i]["order #"] = self.counter()
                    self.pend_orders[i]["trade status"] = "open"
                    self.live_orders[i] = self.pend_orders[i]

                    del self.pend_orders[i]

    def close_order(self, order_number, close_price, close_time, trade_status="close"):

        order = self.live_orders[order_number]

        order["close price"] = close_price
        order["close time"] = close_time
        order["trade status"] = trade_status

        if order["type"] == "buy":

            order["pnl"] = (close_price - order["open price"]) * order["lots"] * self.size

        elif order["type"] == "sell":

            order["pnl"] = (order["open price"] - close_price) * order["lots"] * self.size

        self.closed_orders[order_number] = order

        del self.live_orders[order_number]

    def close_orders(self, order_number_lst, trade_status="close"):

        for i in order_number_lst:

            if self.live_orders[i]["type"] == "buy":

                self.close_order(i, self.current_bid, self.current_time, trade_status)

            elif self.live_orders[i]["type"] == "sell":

                self.close_order(i, self.current_ask, self.current_time, trade_status)

    def closeby(self, order_number, order_number_opp, trade_status="closeby"):

        if self.live_orders[order_number]["lots"] != self.live_orders[order_number_opp]["lots"]:
            raise NameError("Invalid lots to closeby")

        self.close_order(order_number, self.live_orders[order_number_opp]["open price"],
                         self.current_time, "closeby")

        self.close_order(order_number_opp, self.live_orders[order_number_opp]["open price"],
                         self.current_time, "closeby")

    def closeby_lst(self, order_number_lst, order_number_opp_lst, trade_status="closeby"):

        order_number_pairs = zip(order_number_lst, order_number_opp_lst)

        for order_number, order_number_opp in order_number_pairs:
            self.closeby(order_number, order_number_opp, trade_status)

    def closeby_all(self, order_number_lst, order_opp_dir, trade_status="closeby"):

        order_number_opp_lst = [k for k, v in self.live_orders.items() if v["type"] == order_opp_dir]

        self.closeby_lst(order_number_lst, order_number_opp_lst, trade_status)

    def close_all(self, trade_status="close"):

        self.close_orders(list(self.live_orders.keys()), trade_status)

    def tpsl_all(self):

        for k in list(self.live_orders.keys()):

            if self.live_orders[k]["TP"] != None:

                if self.current_bid >= self.live_orders[k]["TP"] and self.live_orders[k]["type"] == "buy":

                    self.close_order(k, self.live_orders[k]["TP"], self.current_time, trade_status="tp")

                elif self.current_ask <= self.live_orders[k]["TP"] and self.live_orders[k]["type"] == "sell":

                    self.close_order(k, self.live_orders[k]["TP"], self.current_time, trade_status="tp")

        for k in list(self.live_orders.keys()):

            if self.live_orders[k]["SL"] != None:

                if self.current_bid <= self.live_orders[k]["SL"] and self.live_orders[k]["type"] == "buy":

                    self.close_order(k, self.live_orders[k]["SL"], self.current_time, trade_status="sl")

                elif self.current_ask >= self.live_orders[k]["SL"] and self.live_orders[k]["type"] == "sell":

                    self.close_order(k, self.live_orders[k]["SL"], self.current_time, trade_status="sl")

    def summary(self):

        summ = {}
        closed_orders_df = self.get_hist_orders("closed")

        # no. of orders
        summ["total trade"] = len(closed_orders_df)
        summ["total buy trade"] = len(closed_orders_df[closed_orders_df["type"] == "buy"])
        summ["total sell trade"] = len(closed_orders_df[closed_orders_df["type"] == "sell"])

        # % of buy & sell orders
        summ["% buy trade"] = summ["total buy trade"] / summ["total trade"]
        summ["% sell trade"] = summ["total sell trade"] / summ["total trade"]
        summ["% trade"] = summ["% buy trade"] + summ["% sell trade"]

        # no. of closed orders
        summ["total buy closed order"] = len(closed_orders_df.loc[(closed_orders_df["type"] == "buy") \
                                                                  & (closed_orders_df[
                                                                         "trade status"] != "close by stop")])
        summ["total sell closed order"] = len(closed_orders_df.loc[(closed_orders_df["type"] == "sell") \
                                                                   & (closed_orders_df[
                                                                          "trade status"] != "close by stop")])
        summ["total closed order"] = summ["total buy closed order"] + summ["total sell closed order"]

        # no. of unclosed orders
        summ["total buy unclosed order"] = summ["total buy trade"] - summ["total buy closed order"]
        summ["total sell unclosed order"] = summ["total sell trade"] - summ["total sell closed order"]
        summ["total unclosed order"] = summ["total trade"] - summ["total closed order"]

        # % of unclosed orders
        summ["% buy unclosed order"] = summ["total buy unclosed order"] / summ["total buy trade"] \
            if summ["total buy trade"] > 0 else None
        summ["% sell unclosed order"] = summ["total sell unclosed order"] / summ["total sell trade"] \
            if summ["total sell trade"] > 0 else None
        summ["% unclosed order"] = summ["total unclosed order"] / summ["total trade"] \
            if summ["total trade"] > 0 else None

        buy_pnl = closed_orders_df.loc[closed_orders_df["type"] == "buy", "pnl"]
        sell_pnl = closed_orders_df.loc[closed_orders_df["type"] == "sell", "pnl"]

        # total pnl
        summ["total buy pnl"] = buy_pnl.sum()
        summ["total sell pnl"] = sell_pnl.sum()
        summ["total pnl"] = summ["total buy pnl"] + summ["total sell pnl"]

        # total profit
        summ["total buy profit"] = closed_orders_df.loc[(closed_orders_df["type"] == "buy") \
                                                        & (closed_orders_df["pnl"] > 0), \
                                                        "pnl"].sum()
        summ["total sell profit"] = closed_orders_df.loc[(closed_orders_df["type"] == "sell") \
                                                         & (closed_orders_df["pnl"] > 0), \
                                                         "pnl"].sum()
        summ["total profit"] = summ["total buy profit"] + summ["total sell profit"]

        # total loss
        summ["total buy loss"] = closed_orders_df.loc[(closed_orders_df["type"] == "buy") \
                                                      & (closed_orders_df["pnl"] < 0), \
                                                      "pnl"].sum()
        summ["total sell loss"] = closed_orders_df.loc[(closed_orders_df["type"] == "sell") \
                                                       & (closed_orders_df["pnl"] < 0), \
                                                       "pnl"].sum()
        summ["total loss"] = summ["total buy loss"] + summ["total sell loss"]

        # P/L
        summ["buy P/L"] = -summ["total buy profit"] / summ["total buy loss"] \
            if summ["total buy loss"] != 0 else 0
        summ["sell P/L"] = -summ["total sell profit"] / summ["total sell loss"] \
            if summ["total sell loss"] != 0 else 0
        summ["P/L"] = -summ["total profit"] / summ["total loss"] \
            if summ["total loss"] != 0 else 0

        # average pnl per trade
        summ["ave pnl per win trade"] = closed_orders_df \
            .loc[closed_orders_df["pnl"] > 0, "pnl"].mean()
        summ["ave pnl per loss trade"] = closed_orders_df \
            .loc[closed_orders_df["pnl"] < 0, "pnl"].mean()
        summ["buy ave pnl per win trade"] = buy_pnl[buy_pnl > 0].mean()
        summ["sell ave pnl per win trade"] = sell_pnl[sell_pnl > 0].mean()
        summ["buy ave pnl per loss trade"] = buy_pnl[buy_pnl < 0].mean()
        summ["sell ave pnl per loss trade"] = sell_pnl[sell_pnl < 0].mean()
        summ["ave pnl per buy trade"] = buy_pnl.mean()
        summ["ave pnl per sell trade"] = sell_pnl.mean()
        summ["ave pnl per trade"] = closed_orders_df["pnl"].mean()

        # max profit    
        summ["max buy profit"] = buy_pnl.max()
        summ["max sell profit"] = sell_pnl.max()
        summ["max profit"] = max(summ["max buy profit"], summ["max sell profit"])

        # max loss
        summ["max buy loss"] = buy_pnl.min()
        summ["max sell loss"] = sell_pnl.min()
        summ["max loss"] = min(summ["max buy loss"], summ["max sell loss"])

        # no. of win orders    
        summ["buy win orders"] = len(closed_orders_df.loc[(closed_orders_df["type"] == "buy") \
                                                          & (closed_orders_df["pnl"] > 0)])
        summ["sell win orders"] = len(closed_orders_df.loc[(closed_orders_df["type"] == "sell") \
                                                           & (closed_orders_df["pnl"] > 0)])
        summ["win orders"] = summ["buy win orders"] + summ["sell win orders"]

        # %win
        summ["buy win rate"] = summ["buy win orders"] / summ["total buy closed order"] \
            if summ["total buy closed order"] != 0 else None
        summ["sell win rate"] = summ["sell win orders"] / summ["total sell closed order"] \
            if summ["total sell closed order"] != 0 else None
        summ["win rate"] = summ["win orders"] / summ["total closed order"] \
            if summ["total closed order"] != 0 else None

        pos_buy_cct = cct(buy_pnl > 0)
        pos_sell_cct = cct(sell_pnl > 0)
        neg_buy_cct = cct(buy_pnl < 0)
        neg_sell_cct = cct(sell_pnl < 0)

        def max_cons_sum_id(cctr, max_cct=None):
            end_id = cctr.reset_index(drop=True).idxmax() + 1
            if max_cct == None:
                max_cct = cctr.max()
            start_id = end_id - max_cct
            return (start_id, end_id)

        def max_cont_trend(pnl, direc):
            cum_pnl = pnl.cumsum()
            no_eq = cum_pnl[cum_pnl != cum_pnl.shift(1)]
            local_max = no_eq[(no_eq > no_eq.shift(1)) & (no_eq > no_eq.shift(-1))]
            local_min = no_eq[(no_eq < no_eq.shift(1)) & (no_eq < no_eq.shift(-1))]

            if len(local_max) == 0 or len(local_min) == 0:
                return "No enough trade"

            local_min_index = int(re.findall("\d+", local_min.index[0])[0]) if isinstance(local_min.index[0], str) else \
            local_min.index[0]
            local_max_index = int(re.findall("\d+", local_max.index[0])[0]) if isinstance(local_max.index[0], str) else \
            local_max.index[0]

            if local_min_index < local_max_index:
                if direc == "riseup":
                    max_trend = (local_max.reset_index(drop=True) - local_min.reset_index(drop=True)).max()
                elif direc == "drawdown":
                    if len(local_min) < 1:
                        return "No enough local mins"
                    else:
                        max_trend = -(local_max.reset_index(drop=True) - local_min.reset_index(drop=True)[1:] \
                                      .reset_index(drop=True)).max()
            else:
                if direc == "riseup":
                    if len(local_max) < 1:
                        return "No enough local maxs"
                    else:
                        max_trend = (local_max.reset_index(drop=True)[1:].reset_index(drop=True) - local_min. \
                                     reset_index(drop=True)).max()
                elif direc == "drawdown":
                    max_trend = -(local_max.reset_index(drop=True) - local_min.reset_index(drop=True)).max()

            return max_trend

        if not pos_buy_cct.empty and not pos_sell_cct.empty and not neg_buy_cct.empty and not neg_sell_cct.empty:
            # max consecutive win
            summ["buy max cont. win"] = pos_buy_cct.max()
            summ["sell max cont. win"] = pos_sell_cct.max()
            summ["max cont. win"] = max(summ["buy max cont. win"], summ["sell max cont. win"])

            # max consecutive loss    
            summ["buy max cont. loss"] = neg_buy_cct.max()
            summ["sell max cont. loss"] = neg_sell_cct.max()
            summ["max cont. loss"] = max(summ["buy max cont. loss"], summ["sell max cont. loss"])

            pos_buy_cctid = max_cons_sum_id(pos_buy_cct)
            pos_sell_cctid = max_cons_sum_id(pos_sell_cct)
            neg_buy_cctid = max_cons_sum_id(neg_buy_cct)
            neg_sell_cctid = max_cons_sum_id(neg_sell_cct)

            # max consecutive win profit
            summ["buy max cont. win profit"] = closed_orders_df.loc[(closed_orders_df["type"] == "buy"), "pnl"] \
                                                   .reset_index(drop=True)[pos_buy_cctid[0]:pos_buy_cctid[1]].sum()
            summ["sell max cont. win profit"] = closed_orders_df.loc[(closed_orders_df["type"] == "sell"), "pnl"] \
                                                    .reset_index(drop=True)[pos_sell_cctid[0]:pos_sell_cctid[1]].sum()
            summ["max cont. win profit"] = max(summ["buy max cont. win profit"], summ["sell max cont. win profit"])

            # max consecutive loss lose                                    
            summ["buy max cont. loss lose"] = closed_orders_df.loc[(closed_orders_df["type"] == "buy"), "pnl"] \
                                                  .reset_index(drop=True)[neg_buy_cctid[0]:neg_buy_cctid[1]].sum()
            summ["sell max cont. loss lose"] = closed_orders_df.loc[(closed_orders_df["type"] == "sell"), "pnl"] \
                                                   .reset_index(drop=True)[neg_sell_cctid[0]:neg_sell_cctid[1]].sum()
            summ["max cont. loss lose"] = min(summ["buy max cont. loss lose"], summ["sell max cont. loss lose"])

        # max rise up
        summ["buy max rise up"] = max_cont_trend(buy_pnl, "riseup")
        summ["sell max rise up"] = max_cont_trend(sell_pnl, "riseup")
        summ["max rise up"] = max_cont_trend(closed_orders_df["pnl"], "riseup")

        # max draw down
        summ["buy max draw down"] = max_cont_trend(buy_pnl, "drawdown")
        summ["sell max draw down"] = max_cont_trend(sell_pnl, "drawdown")
        summ["max draw down"] = max_cont_trend(closed_orders_df["pnl"], "drawdown")

        pre_pd = {}
        pre_pd["buy"] = {}
        pre_pd["sell"] = {}
        pre_pd["overall"] = {}

        for k in summ:
            if "buy" in k:
                pre_pd["buy"][k.replace("buy ", "")] = summ[k]
            elif "sell" in k:
                pre_pd["sell"][k.replace("sell ", "")] = summ[k]
            else:
                pre_pd["overall"][k] = summ[k]

        df = pd.DataFrame.from_dict(pre_pd)
        df["index"] = df.index
        df["index"] = df["index"].astype("category")

        sorter = ["total trade", "% trade", "total closed order", "total unclosed order",
                  "% unclosed order", "total pnl", "total profit", "total loss",
                  "P/L", "ave pnl per win trade", "ave pnl per loss trade",
                  "ave pnl per trade", "max profit", "max loss", "win orders", "win rate",
                  "max cont. win", "max cont. loss", "max cont. win profit",
                  "max cont. loss lose", "max rise up", "max draw down"]

        df["index"].cat.set_categories(sorter, inplace=True)
        df = df.sort_values(["index"]).drop("index", axis=1)
        df = df[["buy", "sell", "overall"]]

        return df

    def float_record(self):

        float_buy = [(self.current_bid - v["open price"]) * v["lots"] * self.size \
                     for v in self.live_orders.values() if v["type"] == "buy"]

        float_sell = [(v["open price"] - self.current_ask) * v["lots"] * self.size \
                      for v in self.live_orders.values() if v["type"] == "sell"]

        closed_pnl = [v["pnl"] for v in self.closed_orders.values()]

        float_long = [v["lots"] for v in self.live_orders.values() if v["type"] == "buy"]

        float_short = [v["lots"] for v in self.live_orders.values() if v["type"] == "sell"]

        current_float_pnl = 0
        pos_float_pos = 0
        neg_float_pos = 0

        for pnl in float_buy + float_sell + closed_pnl:
            current_float_pnl += pnl

        for pos in float_long:
            pos_float_pos += pos

        for pos in float_short:
            neg_float_pos += pos

        self.float_pnl[self.current_time] = current_float_pnl
        self.float_pos[self.current_time] = pos_float_pos - neg_float_pos


class Visual_pad:

    def __init__(self, title, time_frame, start_time=None, end_time=None,
                 plot_width=1800, plot_height=900,
                 TOOLS="pan,wheel_zoom,box_zoom,reset,save"):

        self.p = figure(x_axis_type="datetime", tools=TOOLS, plot_width=plot_width,
                        plot_height=plot_height, title=title)
        self.p.xaxis.major_label_orientation = pi / 4
        self.p.grid.grid_line_alpha = 0.3

        self.start_time = start_time
        self.end_time = end_time
        self.time_frame = time_frame
        self.title = title
        self.min_height = None
        self.max_height = None

    def draw_price(self, price_chart):

        # get ohlc df 
        df = price_chart.chart_dict[self.time_frame]

        # cut time range
        if self.start_time != None:
            df = df.loc[df["Datetime"] >= pd.to_datetime(self.start_time)]

        if self.end_time != None:
            df = df.loc[df["Datetime"] <= pd.to_datetime(self.end_time)]

        # define green & red bar
        df_inc = df[df["Close"] > df["Open"]]
        df_dec = df[df["Open"] >= df["Close"]]

        # define width of bars
        w = self.time_frame * 30 * 1000

        # draw high/low line
        self.p.segment(df["Datetime"], df["High"], df["Datetime"], df["Low"], color="black")

        # draw green boxes
        green_box = self.p.vbar("Datetime", w, "Open", "Close", fill_color="green",
                                line_color="black", source=df_inc)

        # draw red boxes
        red_box = self.p.vbar("Datetime", w, "Close", "Open", fill_color="red",
                              line_color="black", source=df_dec)

        # add ohlc information for boxes
        self.price_size = price_format[price_chart.name]

        tool_tips = [("Datetime", "@Datetime{%F %H:%M:%S}"),
                     ("Open", "@Open{" + self.price_size + "}"),
                     ("High", "@High{" + self.price_size + "}"),
                     ("Low", "@Low{" + self.price_size + "}"),
                     ("Close", "@Close{" + self.price_size + "}")]

        formatters = {"Datetime": "datetime"}

        self.p.add_tools(HoverTool(renderers=[green_box, red_box], tooltips=tool_tips,
                                   formatters=formatters))

        # adjust y-axis range of graph
        self.min_height = min(df["Low"])
        self.max_height = max(df["High"])
        self.p.y_range = Range1d(self.min_height, self.max_height)

    def draw_trade_book(self, trade_book, node_size=10):

        tb = trade_book.get_hist_orders("closed")

        if self.start_time != None:
            tb = tb.loc[tb["open time"] >= pd.to_datetime(self.start_time)]

        if self.end_time != None:
            tb = tb.loc[tb["close time"] <= pd.to_datetime(self.end_time)]

        tb_b = tb.loc[tb["type"] == "buy"]
        tb_s = tb.loc[tb["type"] == "sell"]

        for df in [tb_b, tb_s]:

            for t in ["open time", "close time"]:
                df[t] = df[t].apply(lambda x: x.floor(str(self.time_frame) + "min"))

        open_buy = self.p.triangle("open time", "open price", fill_alpha=0.5,
                                   fill_color="blue", size=node_size, source=tb_b)

        open_sell = self.p.triangle("open time", "open price", fill_alpha=0.5,
                                    fill_color="yellow", size=node_size, angle=pi,
                                    source=tb_s)

        close_buy = self.p.triangle("close time", "close price", fill_alpha=0.5,
                                    fill_color="blue", size=node_size, angle=pi,
                                    source=tb_b)

        close_sell = self.p.triangle("close time", "close price", fill_alpha=0.5,
                                     fill_color="yellow", size=node_size,
                                     source=tb_s)

        buy_line = self.p.segment("open time", "open price", "close time", "close price",
                                  line_color="blue", line_alpha=0.5, line_dash="dashed",
                                  source=tb_b)

        sell_line = self.p.segment("open time", "open price", "close time", "close price",
                                   line_color="orange", line_alpha=0.5, line_dash="dashed",
                                   source=tb_s)

        tool_tips = [("Open Time", "@{open time}{%F %H:%M:%S}"),
                     ("Close Time", "@{close time}{%F %H:%M:%S}"),
                     ("Open Price", "@{open price}{" + self.price_size + "}"),
                     ("Close Price", "@{close price}{" + self.price_size + "}"),
                     ("Profit/Loss", "@pnl{0.00}")]

        formatters = {"open time": "datetime", "close time": "datetime"}

        self.p.add_tools(HoverTool(renderers=[open_buy, open_sell, close_buy,
                                              close_sell, buy_line, sell_line],
                                   tooltips=tool_tips, formatters=formatters))

    def draw_tech(self, tech_lst):

        pal = bp.Category10[10]
        i = 0

        render_lst = []

        for t in tech_lst:

            t.dropna(inplace=True)

            if pd.notnull(self.start_time):
                t = t.loc[t.index >= pd.to_datetime(self.start_time)]

            if pd.notnull(self.end_time):
                t = t.loc[t.index <= pd.to_datetime(self.end_time)]

            i = i % 9
            render_lst.append(self.p.line(list(t.index), list(t.values), color=pal[i]))
            i += 1

        tool_tips = [("Datetime", "$x{%F %H:%M:%S}"), ("Value", "$y{" + self.price_size + "}")]
        formatters = {"$x": "datetime"}

        self.p.add_tools(HoverTool(renderers=render_lst, tooltips=tool_tips,
                                   formatters=formatters))

    def draw_vertical_line(self, x_lst, color="red", line_width=2, alpha=0.7):

        y_lst = [self.min_height] * len(x_lst)
        angle_lst = [90] * len(x_lst)

        if pd.notnull(self.start_time):
            x_lst = [x for x in x_lst if x >= pd.to_datetime(self.start_time)]

        if pd.notnull(self.end_time):
            x_lst = [x for x in x_lst if x <= pd.to_datetime(self.end_time)]

        for x in x_lst:
            self.p.ray(x=x_lst, y=y_lst, angle=angle_lst,
                       angle_units="deg", color=color,
                       length=0, alpha=alpha)

    def draw_shadows(self, xs_lst, color="red", alpha=0.3):

        if pd.notnull(self.start_time):
            xs_lst = [x for x in xs_lst if x[0] >= pd.to_datetime(self.start_time) \
                      and x[1] >= pd.to_datetime(self.start_time)]

        if pd.notnull(self.end_time):
            xs_lst = [x for x in xs_lst if x[0] <= pd.to_datetime(self.end_time) \
                      and x[1] <= pd.to_datetime(self.end_time)]

        for x0, x1 in xs_lst:
            self.p.quad(top=self.max_height, bottom=self.min_height,
                        left=x0, right=x1, color=color, alpha=alpha)

    def show_plot(self):

        reset_output()
        output_file(self.title + ".html", title=self.title)
        show(self.p)

    def to_html_comp(self):

        return components(self.p)


def merge_trade_book(trade_book_lst):
    if len(set([tb.name for tb in trade_book_lst])) != 1:
        raise NameError("Trade books must trade the same product!")

    if len(set([tb.float_pnl.keys() for tb in trade_book_lst])) != 1:
        raise NameError("Trade books must trade in the same time frame!")

    new_tb = Trade_book(trade_book_lst[0].name)

    for tb in trade_book_lst:
        new_tb.live_orders = {**new_tb.live_orders, **tb.live_orders}
        new_tb.closed_orders = {**new_tb.closed_orders, **tb.closed_orders}
        new_tb.pend_orders = {**new_tb.pend_orders, **tb.pend_orders}

    new_tb.float_pnl = trade_book_lst[0].float_pnl
    new_tb.float_pos = trade_book_lst[0].float_pos

    for tb in trade_book_lst[1:]:
        new_tb.float_pnl = {new_tb.float_pnl[i] + tb.float_pnl[i] for i in tb.float_pnl.keys()}
        new_tb.float_pos = {new_tb.float_pos[i] + tb.float_pos[i] for i in tb.float_pos.keys()}

    return new_tb
