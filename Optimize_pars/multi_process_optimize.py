# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 10:45:29 2018

@author: Administrator
"""

import datetime
import json
import multiprocessing as mp
import os
import pickle as pk
from importlib import import_module as impmo

from Optimize_pars import opt_class as oc


def opt_looper(str_name, opt, symbol_name, price_chart_dict, opt_rng):
    """ Pack the pars and extract result_dict from Optimizer object.
    Print optimize process at the begin & end for each loop.

    Args:
        str_name: The string of strategy's name.
        opt: A Optimizer object.
        symbol_name: The string of symbol's name.
        price_chart_dict: A dict of Price_chart object to map the prices
            for each symbol.
        opt_rng: A dict of list of all want-to-try value for the inputs
            mean to optimize.

    Return:
        A dict with the result of optimization, extract directly from a
        Optimizer object.
    """

    print(str_name + ": " + symbol_name + " optimize start at " + str(datetime.datetime.now()))

    opt.exhaust_result_pck(price_chart_dict[symbol_name], opt_rng)

    print(str_name + ": " + symbol_name + " optimize end at " + str(datetime.datetime.now()))

    return opt.result_dict


def multi_process_opt(price_chart_pk_loc, opt_result_dump_loc, opt_setting_file_name):
    """
    Using multiple process to run trading logic with different inputs. Dump the result
    as pickle file for each strategy.
    Note that it MUST run under if __name__ == "__main__"!
    """
    # read optimize setting file
    with open(opt_setting_file_name) as f:
        opt_set = json.load(f)

    # loop each strategy settings
    for str_name, str_set in opt_set.items():
        main = impmo("." + str_name, "Optimize_pars.Strategies").main  # import the main function of strategy
        fix_input = str_set["fix_input"]
        symbol_lst = str_set["symbol_lst"]

        # transfer range settings of want-to-optimize inputs from json
        opt_rng = {}
        for k, v in str_set["opt_rng"].items():
            if v[-1] == "range":
                opt_rng[k] = list(range(*v[:-1]))
            elif v[-1] == "range_float":
                opt_rng[k] = [x * v[-2] for x in list(range(*v[:-2]))]
            elif v[-1] == "list":
                opt_rng[k] = v[:-1]

        for k, v in str_set["opt_rng"].items():

            if v[-1] == "range":

                opt_rng[k] = list(range(*v[:-1]))

            elif v[-1] == "range_float":

                opt_rng[k] = [x * v[-2] for x in list(range(*v[:-2]))]

            elif v[-1] == "list":

                opt_rng[k] = v[:-1]

        # create optimizer
        opt = oc.Optimizer(main, fix_input)

        # read price charts
        price_chart_dict = {}
        for symbol_name in symbol_lst:
            with open(price_chart_pk_loc + "\\" + symbol_name + ".pk", "rb") as f:
                price_chart_dict[symbol_name] = pk.load(f)

        # calculate amount of settings can be run
        comb_num = 1
        for i in [len(x) for x in opt_rng.values()]:
            comb_num *= i
        print("Amount of setting combinations: " + str(comb_num))

        # create pars tuple
        par_lst = [(str_name, opt, symbol_name, price_chart_dict, opt_rng) for symbol_name in symbol_lst]

        # multiprocess stimulate begin
        with mp.Pool(mp.cpu_count() - 1) as p:
            opt_result_lst = p.starmap(opt_looper, par_lst)

        # record optimize result in the Optimizer object
        for v in opt_result_lst:
            opt.result_dict = {**opt.result_dict, **v}

        # dump the Optimizer object as pickle file
        if not os.path.exists(opt_result_dump_loc + "\\" + f"{datetime.datetime.now():%Y%m%d}\\"):
            os.makedirs(opt_result_dump_loc + "\\" + f"{datetime.datetime.now():%Y%m%d}\\")

        with open(opt_result_dump_loc + "\\" + f"{datetime.datetime.now():%Y%m%d}\\" + str_name + "_result_at_"
                  + f"{datetime.datetime.now():%Y%m%d}" + ".pk", "wb") as f:
            pk.dump(opt, f)

        print(str_name + " result dumped!")
