# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 16:13:17 2018

@author: Moe
"""

import json
import pickle as pk
from importlib import import_module as impmo
from pathlib import Path

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti

# read setting json
input_dict = json.load(open("bktst_settings_sample.json"))


# indicator visualize map
TECH_IND = {"BBANDS": ti.BBANDS}


for strategy, factors in input_dict.items():
    
    # import strategy func
    main = impmo(".." + strategy, "Strategy.").main
    
    for symbol_name, settings in factors["symbols"].items():

        # fetch price chart 
        price_chart = pk.load(open(str(Path(__file__).parents[1]) \
                                      + "\\Data\\price_charts\\" \
                                      + symbol_name + ".pk", "rb"))
        
        # insert price_chart into settings
        settings["price_chart"] = price_chart
        
        # create back-testing trade book
        tb = main(**settings)
        
        # visualize back-testing result
        panel = tc.Visual_pad(price_chart.name, settings["bar_time_frame"], settings["start_date"], 
                              settings["end_date"])
        panel.draw_price(price_chart)
        panel.draw_trade_book(tb)
        
        for tech_name, tech_pars in factors["tech_ind"].items():
            
            # set tech func
            tech_func = TECH_IND[tech_name]
            
            # set tech func parameters
            pars = {v: settings[k] for k, v in tech_pars["pars"].items()}
            pars["df"] = price_chart.chart_dict[settings["bar_time_frame"]].set_index("Datetime")
        
            # create tech indicator series
            tech_s = tech_func(**pars)
            
            # visualize tech indicators
            graph_lst = [tech_s[graph] for graph in tech_pars["draw"]]
            panel.draw_tech(graph_lst)
            
        panel.show_plot()