# -*- coding: utf-8 -*-
"""
Created on Mon Jan  7 17:19:09 2019

@author: Administrator
"""

import os
import re
import markdown2
import pickle as pk
import pandas as pd
from pathlib import Path

# ================================ settings ==================================

first_n = 10
rank_f = "sharp ratio"

# ============================================================================

# ================================ helpers ===================================

def df2md(df):
    
    cols = df.columns
    df2 = pd.DataFrame([[":---:", ] * len(cols)], columns = cols)
    df3 = pd.concat([df2, df])
    df_text = df3.to_csv(sep = "|", index = False)
    return ("|" + df_text.replace("\n", "|\n|"))[:-1]


def reg_float(seri, dig = 6):
    
    format_str = "{0:." + str(dig) + "f}"
    
    return pd.Series([format_str.format(val) for val in seri], index = seri.index)

# ============================================================================

# read opt results
result_loc = Path(__file__).parent / "opt_results/20190109"
opt_dict = {re.search(".+(?=_result)", file_name).group(0): \
               pk.load(open(result_loc / file_name, "rb")) \
               for file_name in [pk_f for pk_f in os.listdir(result_loc) if ".pk" in pk_f]}

# create result dict
opt_tables = {}

# loop & pick up the best 10 results (sort by sharp ratio)
for stra, opt in opt_dict.items():
    
    opt_tables[stra] = {}
    
    for symbol_name, exh_pars in opt.result_dict.items():

        if symbol_name == "settings":

            continue
        
        opt_tables[stra][symbol_name] = pd.DataFrame(exh_pars).T.nlargest(first_n, rank_f)
        
        col_float = {"P/L": 2, "sharp ratio": 6, "win rate": 2, "pnl": 2, "no. of trade": 0}

        for col, dig in col_float.items():

            opt_tables[stra][symbol_name][col] = reg_float(opt_tables[stra][symbol_name][col], dig)

        opt_tables[stra][symbol_name] = pd.concat([pd.DataFrame(opt.result_dict["settings"]).loc[opt_tables[stra][symbol_name].index], 
                                                   opt_tables[stra][symbol_name]], 
                                                  axis = 1, sort = True) \
                                          .sort_values("sharp ratio", ascending = False)

# record result by markdown-to-html
md_text = "# Optimize Report\n"

for stra_name, symb_result in opt_tables.items():
    
    md_text += "## " + stra_name + "\n" + \
               "Fix inputs: \n" + \
               "> " + str(opt_dict[stra_name].fix_dict) + "\n\n"
                                  
    for symb, result in symb_result.items():
        
        md_text += "### " + symb + "\n\n" + \
                   df2md(result) + "\n\n"

# save html
with open("Optimize Result.html", "w+") as f:
    
    f.write(markdown2.markdown(md_text, extras = ["code-friendly", "tables"]))   