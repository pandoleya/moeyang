# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 17:21:37 2019

@author: Moe Yang
"""

import json
from Read_server_ticks import update_price_charts as upc
from Optimize_pars import multi_process_optimize as mpo

if __name__ == "__main__":

    # read initial file to get settings
    with open("initial.json") as f:
        initial_dict = json.load(f)

    # prepare paths for important files
    root_path = initial_dict["root"]["path"]
    database_info = initial_dict["price_charts"]["price_server_database_info"]
    price_log_path = root_path + initial_dict["price_charts"]["paths"]["price_charts_reader_loc"]
    price_pkl_path = root_path + initial_dict["price_charts"]["paths"]["price_charts_pkl_loc"]
    opt_cal_path = root_path + initial_dict["opt_cal"]["paths"]["opt_cal_main_loc"]
    opt_results_pkl_path = root_path + initial_dict["opt_cal"]["paths"]["opt_results_pkl_loc"]
    opt_settings_path = opt_cal_path + initial_dict["opt_cal"]["opt_settings_file"]

    # prepare things would do
    update_price_lock = initial_dict["root"]["update_price_lock"]
    opt_cal_lock = initial_dict["root"]["opt_cal_lock"]

    # update prices from server
    if update_price_lock:
        upc.update_prices(database_info, price_pkl_path, price_log_path)

    # optimize calculation
    if opt_cal_lock:
        mpo.multi_process_opt(price_pkl_path, opt_results_pkl_path, opt_settings_path)
